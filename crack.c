#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE* dict = fopen(dictionary,"r");

    if(dict != NULL)
    {
        // Loop through the dictionary file, one line
        // at a time.
        while(!feof(dict))
        {
            char psswrd[PASS_LEN];
            fgets(psswrd,PASS_LEN,dict);
            
            char* nLine = strchr(psswrd,'\n');
            if(nLine != NULL)
            {
                *nLine = '\0';
            }
    
            // Hash each password. Compare to the target hash.
            // If they match, return the corresponding password.
            char* hash = md5(psswrd,strlen(psswrd));
            if(strcmp(hash,target)==0)
            {
                return psswrd;
            }
            
            // Free up memory?
            free(hash);
            
        }
    
        fclose(dict);
    }
    else
    {
        printf("Password file failed to open.\n");
    }
    //Returns NULL if it didn't work
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE* hashes = fopen(argv[1],"r");
    
    if(hashes != NULL)
    {
            
        // For each hash, crack it by passing it to crackHash
        while(!feof(hashes))
        {
            char hash[HASH_LEN];
            fgets(hash,HASH_LEN,hashes);
            
            char* psswrdMatch = crackHash(hash,argv[2]);
         
            // Display the hash along with the cracked password:
            //   5d41402abc4b2a76b9719d911017c592 hello   
            if(psswrdMatch != NULL)
            {
                printf("%s %s\n",hash,psswrdMatch);    
            }
        }
        
        // Close the hash file
        fclose(hashes);
    }
    else
    {
        printf("hashes file failed to load.\n");
    }
    // Free up any malloc'd memory?
}
